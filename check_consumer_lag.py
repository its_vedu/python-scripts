#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import json
import urllib2
import argparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument('--threshold', action="store", type=int)
parser.add_argument('--host', action="store", default='xxx', type=str)
parser.add_argument('--service', action="store", type=str)
args = parser.parse_args()

hostname = socket.gethostname().split('.')[0].upper()
host = args.host
service = args.service
url = 'http://' + host + ':8000/v2/kafka/local/consumer/' + hostname + '-scoreupdater-' + service + '/lag'

def return_lag(url):
    lag_count={}
    response = urllib2.urlopen(url)
    output = response.read()
    k = json.loads(output)
    if k['error'] == False:
        for i in k['status']['partitions']:
            lag_count[i['partition']] = i['end']['lag']
        return lag_count

lag = return_lag(url)

if bool(lag):
    for key in lag.keys():
        if lag[key] < args.threshold:
            print "Ok - Partition {0} of {1} consumer has lag of {2}."  .format(key,service,lag[key])
            sys.exit(0)
        elif lag[key] >= args.threshold:
            print "CRITICAL - Partition {0} of {1} consumer has lag of {2}."  .format(key,service,lag[key])
            sys.exit(2)
        else:
          print "UNKNOWN - Partition {0} of {1} consumer has UNEXPECTED lag : {2}."  .format(key,service,lag[key])
          sys.exit(3)
else:
    print "There are errors in the kafka consumer"
    sys.exit(2)
