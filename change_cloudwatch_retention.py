#!/usr/bin/env python3



import boto3
#import argparse
import os



usage = '''This script changes the retention period of CloudWatch log group.
           The Script accepts two optional arguments.
           --retention, changes the retention period. Default is 5.
           --ignore, ignores the log group.'''

#parser = argparse.ArgumentParser(description=usage)
#parser.add_argument("--retention", help="retention in days", type=int)
#parser.add_argument("--ignore", nargs='+', help="log groups to ignore", type=str)
#args = parser.parse_args()

retention_in_days = 7

if "RETENTION_IN_DAYS" in os.environ:
    retention_in_days = int(os.environ['RETENTION_IN_DAYS'])

client = boto3.client('logs')
paginator = client.get_paginator('describe_log_groups')


def get_cwg():
    cwgroup = []
    response_iterator = paginator.paginate(
        PaginationConfig={
            'MaxItems': 500,
            'PageSize': 50
        }
    )
    for cwg in response_iterator:
        for groups in cwg['logGroups']:
            cwgroup.append(groups['logGroupName'])
    return cwgroup


list_cwlg = get_cwg()
if "IGNORE_LOG_GROUPS" in os.environ:
    list_ignored_logs = os.environ['IGNORE_LOG_GROUPS'].split()



def remove_ignored_logs(ignored_logs, original_logs):
    ignore_groups = set(ignored_logs)
    actual_groups = set(original_logs)
    return list(ignore_groups.union(actual_groups) - ignore_groups.intersection(actual_groups))


def change_retention(logs_process):
    for i in logs_process:
        yield i


def lambda_handler(event, context):
    if "IGNORE_LOG_GROUPS" in os.environ:
        final_logs = remove_ignored_logs(list_ignored_logs, list_cwlg)
        for j in change_retention(final_logs):
            client.put_retention_policy(logGroupName=j,
                                    retentionInDays=retention_in_days)
    else: 
        for k in change_retention(list_cwlg):
            client.put_retention_policy(logGroupName=k,
                                retentionInDays=retention_in_days)