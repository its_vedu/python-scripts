#!/usr/bin/env python

"""
check_redis.py: Nagios plugin for checking a redis server.

"""

from optparse import OptionParser
import sys
import redis
import argparse

# Constants
EXIT_OK = 0
EXIT_WARN = 1
EXIT_CRIT = 2
EXIT_UNKNOWN = 3


class RedisCheck(object):
    def __init__(self, host, port, password=None, clientsWarn=None,
                 clientsCrit=None, memWarn=None, memCrit=None, upWarn=None,
                 upCrit=None, slavesWarn=None, slavesCrit=None,
                 evikeysWarn=None, evikeysCrit=None):
        self.status = False
        self.message = False
        self.host = host
        self.port = port
        self.password = password
        self.clientsWarn = clientsWarn
        self.clientsCrit = clientsCrit
        self.slavesWarn = slavesWarn
        self.slavesCrit = slavesCrit
        self.memWarn = float(memWarn)
        self.memCrit = float(memCrit)
        self.upWarn = upWarn
        self.upCrit = upCrit
        self.evikeysWarn = evikeysWarn
        self.evikeysCrit = evikeysCrit
        self._fetchInfo()

    def _setStatus(self, status):
        """ Set the status only, if new status is not lower than current status"""
        if self.status == EXIT_CRIT or self.status == EXIT_WARN:
        pass
    else:
            self.status = status

    def _setMessage(self, message):
        if self.message:
            self.message += "\n"
            self.message += message
        else:
            self.message = message

    def _exit(self):
        print self.message
        sys.exit(self.status)

    def error_exit(self):
        print self.message
        sys.exit(self.status)

    def _fetchInfo(self):
        try:
            self.info = redis.Redis(host=self.host, port=self.port,
                                    password=self.password).info()
        except redis.ConnectionError:
            self._setStatus(EXIT_CRIT)
            self._setMessage("Can't connect to %s:%s" % (self.host, self.port))
            self.error_exit()

    def getStatus(self):
        return self.status

    def getUptime(self):
        uptime = int(self.info['uptime_in_seconds'])

        ret = {}
        ret['d'] = uptime / 86400
        ret['h'] = (uptime % 86400) / 3600
        ret['m'] = (uptime % 3600) / 60
        ret['s'] = uptime

        return ret

    def getConnectedClients(self):
        return self.info['connected_clients']

    def getConnectedSlaves(self):
        return self.info['connected_slaves']

    def getEvictedKeys(self):
        return self.info['evicted_keys']

    def getUsedMem(self):
        return "%.2f" % float(self.info['used_memory'] / 1024.0 / 1024.0)

    def getLastSave(self):
        return self.info['last_save_time']

    def checkUptime(self):
        uptime = self.getUptime()

        if uptime['s'] < self.upCrit:
            self._setMessage("CRITICAL: Uptime is %s seconds" % (uptime['s']))
            self._setStatus(EXIT_CRIT)
        elif uptime['s'] < self.upWarn:
            self._setMessage("WARNING: Uptime is %s minutes" % (uptime['m']))
            self._setStatus(EXIT_WARN)
        else:
            days = 'days'
            if uptime['d'] == 1:
                days = 'day'
            self._setMessage("OK: Uptime is %s %s, %s:%s h" % (uptime['d'], days,
                                                            uptime['h'],
                                                            uptime['m']))
            self._setStatus(EXIT_OK)

    def checkMemory(self):
        mem = float(self.getUsedMem())

        if mem > self.memCrit:
            self._setMessage("CRITICAL: Mem Usage %s is > than Critical %s" % (mem, self.memCrit))
            ret = EXIT_CRIT
        elif mem >  self.memWarn:
            self._setMessage("WARNING: Mem Usage %s is > than Warning %s" % (mem, self.memWarn))
            ret = EXIT_WARN
        else:
            self._setMessage("OK: Used Memory: %s MB" % (mem))
            ret = EXIT_OK

        self._setStatus(ret)

    def checkConnectedClients(self):
        clients = self.getConnectedClients()
        if clients > self.clientsCrit:
            self._setMessage("CRITICAL: Connected Clients %s is > than Critical %s" % (clients, self.clientsCrit))
            ret = EXIT_CRIT
        elif clients > self.clientsWarn:
            self._setMessage("WARNING: Connected Clients %s is > than Warning %s" % (clients, self.clientsWarn))
            ret = EXIT_WARN
        else:
            self._setMessage("OK: Connected Clients: %s" % (clients))
            ret = EXIT_OK

        self._setStatus(ret)

    def checkEvictedKeys(self):
        evicted_keys = self.getEvictedKeys()

        if evicted_keys > self.evikeysCrit:
           self._setMessage("CRITICAL: Evicted keys %s is > than Critical %s" % (evicted_keys, self.evikeysCrit))
           ret = EXIT_CRIT
        elif evicted_keys > self.evikeysWarn:
           self._setMessage("WARNING: Evicted keys %s is > than Warning %s" % (evicted_keys, self.evikeysWarn))
           ret = EXIT_WARN
        else:
             self._setMessage("OK: Evicted Keys: %s"  % (evicted_keys))
             ret = EXIT_OK

        self._setStatus(ret)


    def checkConnectedSlaves(self):
        slaves = self.getConnectedSlaves()

        if slaves < self.slavesCrit:
            self._setMessage("CRITICAL: Connected slaves %s are > than Critical %s" % (slaves, self.slavesCrit))
            ret = EXIT_CRIT
        elif slaves < self.slavesWarn:
            self._setMessage("WARNING: Connected slaves %s are > than Warning %s" % (slaves, self.slavesWarn))
            ret = EXIT_WARN
        else:
            self._setMessage("OK: Connected Slaves: %s" % (slaves))
            ret = EXIT_OK

        self._setStatus(ret)

    def runChecks(self):
        self.checkUptime()
        self.checkMemory()
        self.checkConnectedClients()
        self.checkConnectedSlaves()
#        self.checkEvictedKeys()

    def check(self):
        self.runChecks()
    stat = self.getStatus()
        self._exit()
    sys.exit(self.status)

def main():
    PARSER = argparse.ArgumentParser(description='Collects Redis Stats for an instance')
    PARSER.add_argument("--port",
                        required=True,
                        type=int,
                        help='Port at which the redis runs.')
    args = PARSER.parse_args()
    redisCheck = RedisCheck('localhost', port=args.port,  memWarn=42000, memCrit=45000, upWarn=300,
                            upCrit=60, clientsWarn=3900, clientsCrit=4000,
                            slavesWarn=0, slavesCrit=0,
                            evikeysWarn=250, evikeysCrit=500)

    redisCheck.check()

if __name__ == '__main__':
    main()
